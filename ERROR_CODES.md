|Error code|  Source file|  Object reference|
|----------|  ----------|  ----------|
|`1000`|  `dx_utilities/array_interface.py`|  `typestr`|
|`1001`|  `dx_utilities/array_interface.py`|  `typestr`|
|`1002`|  `dx_utilities/checks.py`|  `check_positive`|
|`1003`|  `dx_utilities/checks.py`|  `check_non_negative`|
|`1004`|  `dx_utilities/checks.py`|  `check_numerical_value`|
|`1005`|  `dx_utilities/data_structures.py`|  `TolerantDict.__getitem__`|
|`1006`|  `dx_utilities/fields/base.py`|  `BaseLinearField.evaluate_direction`|
|`1007`|  `dx_utilities/fields/mixins.py`|  `DiscreteFieldMixin.construct_point_values`|
|`1008`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.new`|
|`1009`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.create_rectangle_by_dimensions`|
|`1010`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.iter_containing_envelopes`|
|`1011`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.stretch_to_container`|
|`1012`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.annotate`|
|`1013`|  `dx_utilities/linear_algebra.py`|  `PlanarShape.project`|
|`1014`|  `dx_utilities/units.py`|  `PlanarShape.transform_value`|
|`1015`|  `dx_utilities/vectors.py`|  `VectorFactory.from_two_points`|
|`1016`|  `dx_utilities/vectors.py`|  `VectorFactory.from_two_points`|
|`1017`|  `dx_utilities/vectors.py`|  `VectorFactory.from_linestring`|
|`1018`|  `dx_utilities/geometry/planar.py`|  `PlanarShape.assert_validity`|
