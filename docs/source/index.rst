Welcome to the documentation of ``dx-utilities``
================================================

Base utilities for engineering packages
---------------------------------------

Features
^^^^^^^^

* Geometry module on top of `shapely`_.
* Representations of physical fields.
* Linear algebra and numerical integration

  * Utilities on top of `numpy` and `mathutils.Vector`.

* Decorators.
* Exception classes with error-code.
* Data-structures.
* Book-keeping of physical constants and manipulation of units.
* Testing and printing utilities

.. Usage
.. -----
.. .. toctree::
..    chapters/usage

.. toctree::
   :hidden:
   :glob:

   _modules/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _shapely: https://shapely.readthedocs.io/en/stable/index.html
