import numpy as np
import unittest.mock as mock

from math import pi, sqrt
from mathutils import Vector
from shapely.geometry import Point

from dx_utilities.geometry import PlanarShape
from test import DXTestCase


class TestPlanarShape(DXTestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_create_rectangle_by_box(self):
        minxy = (0., 0.)
        maxxy = (1., 1.)

        box = PlanarShape.create_rectangle_by_box(minxy, maxxy)

        # Expected
        centroid = Point(0.5, 0.5)
        area = 1.0
        perimeter = 4.0

        # Checks
        self.assertIsInstance(box, PlanarShape)
        self.assertEqual(box.centroid, centroid)
        self.assertEqual(box.area, area)
        self.assertEqual(box.length, perimeter)

        box = PlanarShape.create_rectangle_by_box(minxy, maxxy, ccw=False)
        self.assertIsInstance(box, PlanarShape)

    def test_create_rectangle_by_dimensions(self):

        # Default
        rectangle = PlanarShape.create_rectangle_by_dimensions(1., 1.)
        expected_bounds = (-0.5, -0.5, 0.5, 0.5)

        self.assertIsInstance(rectangle, PlanarShape)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

        # Set centroid coordinates 
        rectangle = PlanarShape.create_rectangle_by_dimensions(1., 1., (0.5, 0.5))
        expected_bounds = (0., 0., 1.0, 1.0)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

    def test_create_rectangle_by_dimensions_lower_left(self):

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., position='lower-left',
                )
        expected_bounds = (0., 0., 1.0, 1.0)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., origin=(0.5, 0.5), position='lower-left',
                )
        expected_bounds = (0.5, 0.5, 1.5, 1.5)

    def test_create_rectangle_by_dimensions_lower_right(self):

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., position='lower-right',
                )
        expected_bounds = (-1.0, 0.0, 0.0, 1.0)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., origin=(0.5, 0.5), position='lower-right',
                )
        expected_bounds = (-1.5, 0.5, 0.5, 1.5)

    def test_create_rectangle_by_dimensions_upper_right(self):

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                0.2, 0.3, origin=(0.3, 0.3), position='lower-left'
                )
        expected_bounds = (0.3, 0.3, 0.5, 0.6)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., origin=(0.5, 0.5), position='upper-right',
                )
        expected_bounds = (-0.5, -0.5, 0.5, 0.5)

    def test_create_rectangle_by_dimensions_upper_left(self):

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., position='upper-left',
                )
        expected_bounds = (0.0, -1.0, 1.0, 0.0)
        self.assertTupleAlmostEqual(rectangle.bounds, expected_bounds)

        rectangle = PlanarShape.create_rectangle_by_dimensions(
                1., 1., origin=(0.5, 0.5), position='upper-left',
                )
        expected_bounds = (-0.5, -1.0, 1.5, 0.5)

    def test_create_rectangle_by_dimension_raise(self):

        with self.assertRaises(ValueError):
            rectangle = PlanarShape.create_rectangle_by_dimensions(
                    1., 0.5, position='unsupported'
                    )

    def test_create_circle(self):

        circle = PlanarShape.create_circle(r=1.)
        
        self.assertIsInstance(circle, PlanarShape)
        self.assertAlmostEqual(circle.area, pi, delta=0.001*pi)

    def test_offset_convex_hull(self):

        rectangle = PlanarShape.create_rectangle_by_dimensions(1., 1.)

        convex_hull_offset = rectangle.offset_convex_hull(1.)
        self.assertIsInstance(convex_hull_offset, PlanarShape)

    def test_normal(self):
        shape = PlanarShape.create_circle(1.)
        parallel_to_normal = Vector((0., 0., 1.))
        self.assertEqual(abs(parallel_to_normal.dot(shape.normal)), 1.)

    @mock.patch(('dx_utilities.geometry.planar.PlanarShape'
                 '._evaluate_normal_vector'), mock.Mock(return_value=1.))
    def test_cache_normal(self):
        shape = PlanarShape()
        shape.normal
        shape.normal
        shape._evaluate_normal_vector.assert_called_once()

    def test_vector(self):
        box = PlanarShape.create_rectangle_by_box((0.0, 0.0), (10., 10.))
        vbox = box.vector
        self.assertEqual(vbox.magnitude, box.area)
        self.assertEqual(vbox.normalized().dot(box.normal), 1.)

    def test_iter_containing_envelopes(self):
        planarshape = PlanarShape.create_rectangle_by_box((0., 0.), (1., 1.))
        member = PlanarShape.create_rectangle_by_dimensions(
                0.5, 0.5, origin=(0.5, 0.5)
                )
        containing_envelopes = list(
                planarshape.iter_containing_envelopes(member)
                )
        self.assertEqual(len(containing_envelopes), 4)
        for env in containing_envelopes:
            self.assertAlmostEqual(env.area, 0.5625)

        member = PlanarShape.create_rectangle_by_dimensions(
                0.5, 0.5, origin=(0., 0.)
                )
        with self.assertRaises(ValueError):
            for env in planarshape.iter_containing_envelopes(member):
                pass

    def test_minimum_containing_envelope(self):
        planarshape = PlanarShape.create_rectangle_by_box((0., 0.), (1., 1.))
        member = PlanarShape.create_rectangle_by_dimensions(
                0.2, 0.2, origin=(0.3, 0.3)
                )
        expected_area = 0.16
        mce = planarshape.find_minimum_containing_envelope(member)
        self.assertAlmostEqual(mce.area, expected_area)

    def test_stretch_to_container(self):
        member = PlanarShape.create_rectangle_by_dimensions(
                0.2, 0.2, origin=(0.3, 0.3)
                )
        container = PlanarShape.create_rectangle_by_box((0., 0.), (1., 1.))

        px, py = member.stretch_to_container(container)

        xbounds = (0., 0.2, 0.4, 0.4)
        ybounds = (0.2, 0., 0.4, 0.4)

        self.assertTupleAlmostEqual(px.bounds, xbounds)
        self.assertTupleAlmostEqual(py.bounds, ybounds)

    def test_distance_between_bounds(self):
        source = PlanarShape.create_rectangle_by_box((0., 0.), (1., 1.))
        target = PlanarShape.create_rectangle_by_dimensions(
                0.2, 0.3, origin=(0.3, 0.3), position='lower-left'
                )

        s2t = source.distance_between_bounds(target)
        t2s = target.distance_between_bounds(source)
        expected_distances = (0.3, 0.3, 0.5, 0.4)

        self.assertTupleAlmostEqual(s2t, expected_distances)
        self.assertTupleAlmostEqual(t2s, expected_distances)

    def test_create_by_offset(self):
        circle = PlanarShape.create_circle(r=0.5)
        rect = PlanarShape.create_rectangle_by_dimensions(1., 1.)

        co = PlanarShape.create_by_offset(circle, dx=0.5)
        self.assertAlmostEqual(co.area, pi, places=3)

        ro = PlanarShape.create_by_offset(rect, dx=0.5)
        self.assertAlmostEqual(ro.area, 4.)

        roxy = PlanarShape.create_by_offset(rect, dx=0.5, dy=1.)
        self.assertAlmostEqual(roxy.area, 6.)
        self.assertAlmostEqual(roxy.bx, 2.)
        self.assertAlmostEqual(roxy.by, 3.)

        roxy = PlanarShape.create_by_offset(rect, dy=0.5, dx=1.)
        self.assertAlmostEqual(roxy.area, 6.)
        self.assertAlmostEqual(roxy.by, 2.)
        self.assertAlmostEqual(roxy.bx, 3.)

    def test_project_point_to_boundary(self):
        shape = PlanarShape.create_rectangle_by_dimensions(bx=10., by=10.,
                                                           position='lower-left')
        point = (5., 5.)
        pdir = set()
        for p, dirv in shape.project_to_boundary(point):
            pdir.add((tuple(p), tuple(dirv)))
        expected = set([
                ((10., 5.), (0., 1.)),
                ((0., 5.), (0., -1.)),
                ((5., 10.), (-1., 0.)),
                ((5., 0.), (1., 0.)),
                ])
        self.assertSetEqual(pdir, expected)

        shape = PlanarShape([(0., 0), (2., 0.), (2., 2.), (1., 2.), (0., 1.0)])
        point = (0.5, 1.5)
        points_dirs = shape.project_to_boundary(point)
        self.assertEqual(len(points_dirs), 3)
        pdir = set()
        for p, dirv in shape.project_to_boundary(point):
            pdir.add((tuple(p), tuple(dirv)))

        self.assertTrue((point, (-sqrt(2.), -sqrt(2.))))

    def test_get_principal_direction(self):
        shape = PlanarShape.new(bx=1., by=0.2)
        vx = shape.get_principal_direction(origin=(-1.0, 0.))
        self.assertAlmostEqual(np.linalg.norm(vx), 1.)
        np.testing.assert_array_almost_equal(vx, [1., 0.])
        vx = shape.get_principal_direction(origin=(+0.5, 0.))
        np.testing.assert_array_almost_equal(vx, [-1., 0.])

    def test_align(self):
        shape = PlanarShape.new(bx=1., by=0.2)
        target = (0.5, 0.5)
        origin = (-0.5, 0.)
        rotated = shape.align(target, origin)
        direction = rotated.get_principal_direction(origin)
        np.testing.assert_array_almost_equal(direction, [0.707, 0.707],
                                             decimal=3)
        target = (-0.5, -0.5)
        rotated = shape.align(target, origin)
        direction = rotated.get_principal_direction(origin)
        np.testing.assert_array_almost_equal(direction, [-0.707, -0.707],
                                             decimal=3)
