import unittest

from math import pi
from dx_utilities.geometry.linear import LinearShape


class TestLinearShape(unittest.TestCase):

    def test_create_arc(self):
        center = (0., 0.)
        arc1 = LinearShape.create_arc(center, radius=1.,
                                      end_angle=45., radians=False)
        self.assertIsInstance(arc1, LinearShape)
        self.assertAlmostEqual(arc1.length, pi/4, delta=1e-03*pi/4)
        arc2 = LinearShape.create_arc(center, radius=1.,
                                      end_angle=-45., start_angle=45.,
                                      radians=False)
        self.assertAlmostEqual(arc2.length, pi/2, delta=1e-03*pi/2)
