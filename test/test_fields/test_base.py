import unittest

from mathutils import Vector

from dx_utilities.fields.base import PointValue


class TestPointValue(unittest.TestCase):

    def test_create_point_vector(self):
        pv = PointValue.create_point_vector(
                coords=(0., 0.), vector_value=(3., 0., 4.)
                )
        self.assertIsInstance(pv, PointValue)
        self.assertIsInstance(pv.value, Vector)
        self.assertAlmostEquals(pv.value.magnitude, 5.)
