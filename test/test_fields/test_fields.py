import unittest
import unittest.mock as mock

from dx_utilities.geometry import LinearShape, PlanarShape

import dx_utilities.fields as fields


class TestBaseLinearField(unittest.TestCase):

    @mock.patch('dx_utilities.fields.BaseLinearField.evaluate_direction',
                mock.Mock(return_value=None))
    def test_instantiation(self):
        line = LinearShape([(0., 0.), (0.3, 0.4), (0.9, 0.9)])
        field = fields.BaseLinearField(line)
        self.assertIsInstance(field, fields.BaseLinearField)

        field.evaluate_direction.assert_called_once()

    def test_evaluate_direction(self):
        line = LinearShape([(0., 0.), (0.3, 0.3), (0.9, 0.9)])
        field = fields.BaseLinearField(line)

        from math import sqrt
        self.assertAlmostEqual(sqrt(2.)/2., field.direction.x)
        self.assertAlmostEqual(sqrt(2.)/2., field.direction.y)

        line = LinearShape([(0., 0.), (0.3, 0.0), (0.3, 0.9)])
        with self.assertRaises(ValueError):
            field = fields.BaseLinearField(line)


class TestDiscreteLineField(unittest.TestCase):

    def test_from_coords_and_values(self):
        coordinates = [(0., 0.), (0., 1.), (0., 2.)]
        values = [1., 13., 17.]

        dlf = fields.DiscreteLineField.from_coords_and_values(
                coordinates, values
                )

        self.assertIsInstance(dlf, fields.DiscreteLineField)
        self.assertIsInstance(dlf.geometry, LinearShape)


class TestDiscretePlanarField(unittest.TestCase):

    def test_from_coords_and_values(self):
        coordinates = [
                (0., 0.), (0.1, 0.2), (0.2, 0.2), (1., 0.), (1., 1.), (0., 1.)
                ]

        values = [1., 2., 2., 2., 3., 3.]

        dpf = fields.DiscretePlanarField.from_coords_and_values(
                coordinates, values
                )
        self.assertIsInstance(dpf, fields.DiscretePlanarField)
        self.assertIsInstance(dpf.geometry, PlanarShape)

        expected_geometry = PlanarShape.create_rectangle_by_box(
                (0., 0.), (1., 1.)
                )
        self.assertTrue(dpf.geometry.difference(expected_geometry).is_empty)

        # Test explicit geometry
        explicit_geometry = PlanarShape(coordinates)

        dpf = fields.DiscretePlanarField.from_coords_and_values(
                coordinates, values, explicit_geometry
                )

        self.assertFalse(expected_geometry.difference(dpf.geometry).is_empty)
        self.assertTrue(expected_geometry.contains(dpf.geometry))
