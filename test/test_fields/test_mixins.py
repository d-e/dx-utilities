import unittest

from dx_utilities.fields.mixins import DiscreteFieldMixin, UniformFieldMixin
from dx_utilities.fields import PointValue


class TestUniformFieldMixin(unittest.TestCase):

    def test_replace_value(self):
        field = UniformFieldMixin(geometry=None, value=0.)
        field.replace_value(1.)
        self.assertAlmostEqual(field.value, 1.)

    def test_add_value(self):
        field = UniformFieldMixin(geometry=None, value=3.)
        field.add_value(1.)
        self.assertAlmostEqual(field.value, 4.)


class TestDiscreteFieldMixin(unittest.TestCase):

    def test_construct_point_values(self):
        coordinates = [(0., 0.), (0., 1.), (0., 2.)]
        values = [1., 13., 17.]

        point_values = DiscreteFieldMixin.construct_point_values(
                coordinates, values
                )

        for i, pv in enumerate(point_values):
            self.assertIsInstance(pv, PointValue)
            self.assertEquals(pv.value, values[i])

        with self.assertRaises(ValueError):
            values.append(18.)
            point_values = DiscreteFieldMixin.construct_point_values(
                    coordinates, values
                    )
