# Copyright (C) 2019 demetriou engineering ltd.
# Part of `dx-utilities`, licensed under the AGPLv3+
"""Global physical constants."""


__all__ = ['g']


#: Acceleration of gravity [m/s/s]
g = 9.81
